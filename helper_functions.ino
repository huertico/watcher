/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/
const String error_filepath = "/last_error";
const String config_path = "/.huertico.conf";

String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
  if (bytes < 1024) {
    return String(bytes) + "B";
  } else if (bytes < (1024 * 1024)) {
    return String(bytes / 1024.0) + "KB";
  } else if (bytes < (1024 * 1024 * 1024)) {
    return String(bytes / 1024.0 / 1024.0) + "MB";
  }
}

void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
  set_error("");
}

bool loadConfigs() {
  bool result = true;
  if (SPIFFS.exists(config_path)) {
    //file exists, reading and loading
    Serial.println("reading config file");
    File conf_file = SPIFFS.open(config_path, "r");
    if (conf_file) {
      Serial.println("opened config file");
      size_t fsize = conf_file.size();
      // Allocate a buffer to store contents of the file.
      std::unique_ptr<char[]> buf(new char[fsize]);

      conf_file.readBytes(buf.get(), fsize);
      //DynamicJsonBuffer jsonBuffer;
      //JsonObject& json = jsonBuffer.parseObject(buf.get());
      DynamicJsonDocument jsonDoc(1024);
      deserializeJson(jsonDoc, buf.get());
      serializeJson(jsonDoc, Serial);
      //json.printTo(Serial);
      Serial.println("");
      //if (json.success()) {
      if (!jsonDoc.isNull()) {
        JsonObject json = jsonDoc.as<JsonObject>();
        Serial.println("\nparsed json");
        mqtt_server = json["mqtt_server"].as<String>();
        if (json["mqtt_port"]) {
          mqtt_port = json["mqtt_port"];
        }
        sleep_time = json["sleep_time"];
      } else {
        Serial.println("failed to load json config");
        result = false;
      }
      conf_file.close();
    }
  } else {
    Serial.println("No json config");
    result = false;
  }

  return result;
}

bool saveConfigs() {
  //TODO: Fix check config data (sanitaze)
  DynamicJsonDocument json(1024);
  json["mqtt_server"] = mqtt_server;
  json["mqtt_port"] = mqtt_port;
  json["sleep_time"] = sleep_time;

  File conf_file = SPIFFS.open(config_path, "w");
  if (!conf_file) {
    Serial.println("failed to open config file for writing");
    return false;
  }
  Serial.println("Saved Json: ");
  serializeJson(json, Serial);
  Serial.println("");
  serializeJson(json, conf_file);
  conf_file.close();
  return true;
}

void set_error (String error_text) {
  Serial.println("Save Error Message: "+error_text);
  File last_error_file = SPIFFS.open(error_filepath, "w");
  if(last_error_file){
    last_error_file.write((uint8_t *)error_text.c_str(), error_text.length()+1);
    last_error_file.close();
  }
}

String get_error () {
  String error_message;
  if (SPIFFS.exists(error_filepath)) {
    File last_error_file = SPIFFS.open(error_filepath, "r");
    if(last_error_file){
      size_t fsize = last_error_file.size();
      // Allocate a buffer to store contents of the file.
      std::unique_ptr<char[]> buf(new char[fsize]);

      last_error_file.readBytes(buf.get(), fsize);
      error_message = buf.get();
      last_error_file.close();
    }
  }
  Serial.println("Get Error Message: "+error_message);
  return error_message;
}
