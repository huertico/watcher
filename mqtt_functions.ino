#define MQTT_DEBUG
#define MQTT_ONE_PUSH true

//Topics
const String moisture_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/moisture";  // ToDo: add ChipID to get data from diferente Hueticos
const String light_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/light";
const String humidity_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/humidity";
const String temperature_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/temperature";
const String data_topic = "huertico/"+String(ESP.getChipId(), HEX)+"/data";
const String register_topic = "huertico/register";


/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/
void RegisterToMQTT() {
  MQTTConnect();

  Adafruit_MQTT_Publish data_pub = Adafruit_MQTT_Publish(mqtt, register_topic.c_str());
  //                       doc has 2 elemets
  const size_t capacity = JSON_OBJECT_SIZE(2);
  DynamicJsonDocument doc(capacity);
  doc["id"] = String(ESP.getChipId(), HEX);
  doc["type"] = HUERTICO_TYPE;

  String serialized_data = "";
  serializeJson(doc, serialized_data);
  Serial.println("Publishing register JSON data");
  if (!data_pub.publish(serialized_data.c_str())) {
    Serial.println("Error publishing register JSON data");
  }
  if (! mqtt->ping()) {
    mqtt->disconnect();
  }
}

void ProcessMQTT() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).
  MQTTConnect();

  if (MQTT_ONE_PUSH) {
    Adafruit_MQTT_Publish data_pub = Adafruit_MQTT_Publish(mqtt, data_topic.c_str());

    //                       doc has 3 elemets    nested need 1 elemet  data has 4 elements
    const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(4);
    DynamicJsonDocument doc(capacity);
    doc["id"] = String(ESP.getChipId(), HEX);
    doc["type"] = HUERTICO_TYPE;
    JsonObject data = doc.createNestedObject("data");
    data["light"] = light_value;
    data["moisture"] = moisture_value;
    data["humidity"] = humidity_value;
    data["temperature"] = temperature_value;

    String serialized_data = "";
    serializeJson(doc, serialized_data);
    Serial.println("Publishing sensors data all in one JSON");
    if (!data_pub.publish(serialized_data.c_str())) {
      Serial.println("Error publishing JSON data");
    }
  } else {
    // Now we can publish stuff!
    Adafruit_MQTT_Publish moisture_pub = Adafruit_MQTT_Publish(mqtt, moisture_topic.c_str());
    Adafruit_MQTT_Publish light_pub = Adafruit_MQTT_Publish(mqtt, light_topic.c_str());
    Adafruit_MQTT_Publish humidity_pub = Adafruit_MQTT_Publish(mqtt, humidity_topic.c_str());
    Adafruit_MQTT_Publish temperature_pub = Adafruit_MQTT_Publish(mqtt, temperature_topic.c_str());
    Serial.println("Publishing sensors data");
    if (!moisture_pub.publish(moisture_value)) {
      Serial.println("Error publishing moisture data");
    }
    if (!light_pub.publish(light_value)) {
      Serial.println("Error publishing light data");
    }
    if (!isnan(humidity_value)) {
      if (!humidity_pub.publish(humidity_value)) {
        Serial.println("Error publishing humidity data");
      }
  
    }
    if (!isnan(temperature_value)) {
      if (!temperature_pub.publish(temperature_value)) {
        Serial.println("Error publishing temperature data");
      }
    }
  }
  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  if (! mqtt->ping()) {
    mqtt->disconnect();
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
bool MQTTConnect() {
  // Stop if no MQTT
  if (! mqtt) {
    return false;
  }
  // Stop if already connected.
  if (mqtt->connected()) {
    return true;
  }

  Serial.print("Connecting to MQTT... ");

  int8_t ret;
  uint8_t retries = 3;
  while ((ret = mqtt->connect()) != 0 && retries > 0) { // connect will return 0 for connected
    Serial.println(mqtt->connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt->disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
  }

  if (mqtt->connected()) {
    Serial.println("MQTT Connected!");
    return true;
  }
  Serial.println("MQTT connection failed!");
  return false;
}
