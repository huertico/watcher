/*__________________________________________________________SENSORS_FUNCTIONS__________________________________________________________*/
void sensorsOff() {
  // power-down sensors
  digitalWrite(moisture_vcc, LOW);
  digitalWrite(light_vcc, LOW);
  digitalWrite(dht_vcc, LOW);
}

void readSensors() {
  sensorsOff();
  delay(100);

  digitalWrite(dht_vcc, HIGH);         // power up sensor
  delay(2500);
  humidity_value = dht.readHumidity(); // read humidity
  temperature_value = dht.readTemperature(); //read temperature
  // Check if any reads failed
  if (isnan(humidity_value) || isnan(temperature_value)) {
    Serial.println("Failed to read from DHT sensor!");
  } 
  digitalWrite(dht_vcc, LOW);         // power down sensor

  digitalWrite(moisture_vcc, HIGH);            // power up sensor
  delay(500);
  moisture_value = analogRead(moisture_sensor);
  //moisture_value =  (moisture_value * 100) / 1024; // read moisture
  digitalWrite(moisture_vcc,LOW);              //power down sensor
  
  digitalWrite(light_vcc,HIGH);          // power up sensor
  delay(500);
  light_value = analogRead(light_sensor); // read light
  //light_value = 1024 - light_value; // read light
 
  sensorsOff();
}

void sensors_test(){
  delay(100);
  Serial.println("START TEST!");
  
  if (!isnan(humidity_value)) {
    Serial.print("humidity: ");
    Serial.println(humidity_value);
  }
  if (!isnan(temperature_value)){
    Serial.print("temperature: ");
    Serial.println(temperature_value);
  }
  Serial.print("Moisture: ");
  Serial.println(moisture_value);
  Serial.print("light: ");
  Serial.println(light_value);

}

