#define SENSOR_DEBUG false
#define HUERTICO_TYPE "earth"
#include <ESP8266WiFi.h>    //https://github.com/esp8266/Arduino
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>   //https://github.com/tzapu/WiFiManager
#include <FS.h>            //https://github.com/esp8266/arduino-esp8266fs-plugin // https://www.instructables.com/id/Using-ESP8266-SPIFFS/
#include <ArduinoJson.h>   //https://github.com/bblanchon/ArduinoJson/
#include "Adafruit_MQTT.h" //https://github.com/adafruit/Adafruit_MQTT_Library/
#include "Adafruit_MQTT_Client.h"
#include <DHT.h>
// Adafruit DHT library needs a hack: https://github.com/adafruit/DHT-sensor-library/issues/48

// SENSOR Vcc GPIO //
#define moisture_vcc 4 //Wemos D2
#define light_vcc 0    //Wemos D3
#define dht_vcc 2      //Wemos D4

// SENSOR Data read GPIO //
#define moisture_sensor A0
#define light_sensor A0
#define dht_sensor 5   //Wemos D1

// Huertico Satellite Names and Topics //
const String sensor_name = "Huertico_" + String(ESP.getChipId(), HEX); // Huertico Sensor Name
const String mdns_name = "huertico" + String(ESP.getChipId(), HEX); // Domain name for the mDNS responder


// SENSOR Data values //
int moisture_value, light_value;
float humidity_value, temperature_value;

// Satellite Configs //
String mqtt_server = "huerticomqtt.local";
int mqtt_port = 1883;
int sleep_time = 60000; // sleep 1 minute
bool mqtt_failed = false;
bool shouldSaveConfig = false;

// Objects init //
DHT dht(dht_sensor, DHT11);
ESP8266WebServer server(80);
File fs_upload_file;
WiFiClient mqtt_wclient;  // Create an ESP8266 WiFiClient class to connect to the MQTT server.
//WiFiClientSecure mqtt_wclient; // or... use WiFiFlientSecure for SSL
Adafruit_MQTT_Client* mqtt;

/*__________________________________________________________MAIN_FUNCTIONS__________________________________________________________*/

void setup() {
  Serial.begin(115200);        // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println("\r\n");

  //setup pinmode
  pinMode(moisture_vcc, OUTPUT);
  pinMode(light_vcc, OUTPUT);
  pinMode(dht_vcc, OUTPUT);
  pinMode(A0, INPUT); //Analog for light and moisture
  dht.begin();       // setup DHT

  if ( ! SENSOR_DEBUG ) {
    startSPIFFS();
    startWiFi();
    startMQTT();
    // if no MQTT start Server
    if (! mqtt || ! mqtt->connected()) {
      set_error("Can not connect to MQTT");
      ESP.reset();
    }
  }
}

void loop() {
  if ( SENSOR_DEBUG ) {
    /*
     * Use this section to test sensors and data
     * without mqtt an wifi 
     */
    readSensors();
    sensors_test();
  } else if (mqtt_failed || ! MQTTConnect()) {
    server.handleClient(); // run the server if no MQTT
  } else {
    readSensors();
    ProcessMQTT();
    Serial.println("Wait 10 min");
    //delay(600000);
    delay(500);
  }
}
