Watcher
=======

![Sensors Module](https://gitlab.com/huertico/repo-images/raw/master/earth.png)

Open Hardware/Software for automated plant growing, sensors module (Watcher).

This repository contains code for an [Internet of Things](https://en.wikipedia.org/wiki/Internet_of_Things) (*IoT*) module responsible for sense enviromental conditions, such as soil moisture, light and air temperature and humidity.

This module reads sensors data and sends it to a [Huertico Server](https://gitlab.com/huertico/server/) which then takes decisions accordingly.

The resulting system is a server who is responsible for read humidity, temperature and light sensors values, and that instructs water sources to be opened when some threshold limit is reached (see [infrastructure](#infrastructure)).

Ingredients
-----------

![Open Hardware](https://gitlab.com/huertico/repo-images/raw/master/openhardware.png)
![IoT](https://gitlab.com/huertico/repo-images/raw/master/iot.png)
[![arduino](https://gitlab.com/huertico/repo-images/raw/master/arduino.png)](https://www.arduino.cc)
[![ESP8266](https://gitlab.com/huertico/repo-images/raw/master/esp8266.png)](https://www.espressif.com/en/products/hardware/esp8266ex/overview)

Requirements
------------

Some Arduino development tool like [Arduino IDE](https://www.arduino.cc/en/Main/Software) or [PlatformIO](https://platformio.org/).

License
-------

GPLv3. See the [LICENSE](https://gitlab.com/huertico/server/raw/master/LICENSE) file for more details.

Author Information
------------------

This repository was created by [![constrict0r](https://gitlab.com/huertico/repo-images/raw/master/constrict0r.png)](https://geekl0g.wordpress.com/author/constrict0r) and [![valarauco](https://gitlab.com/huertico/repo-images/raw/master/valarauco.png)](https://twitter.com/valarauco).

Enjoy!!

![enjoy](https://gitlab.com/huertico/repo-images/raw/master/huertico.png)